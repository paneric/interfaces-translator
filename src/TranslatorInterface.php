<?php

declare(strict_types=1);

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Paneric\Interfaces\Translator;

use InvalidArgumentException;

interface TranslatorInterface
{
    /**
     * Returns the default locale.
     */
    public function getLocale(): string;

    /**
     * @param string      $id         The message id (may also be an object that can be cast to string)
     * @param array       $parameters An array of parameters for the message
     * @param string|null $domain     The domain for the message or null to use the default
     * @param string|null $locale     The locale or null to use the default
     *
     * @throws InvalidArgumentException If the locale contains invalid characters
     */
    public function trans(string $id, array $parameters = [], string $domain = null, string $locale = null): string;

    /**
     * @throws InvalidArgumentException
     */
    public function dict(string $routePath): array;
}
